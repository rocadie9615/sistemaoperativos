/**
*Practica 02 
*Nombre: Diego Rosado Cabrera
*nmc: 314293804
*/

#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define BLOCKTAM 1024

#include <sys/types.h>

struct disco{
  char  nombre [14];
  int fd;
  int tam;
  char bitMap[2048];
};

struct disco * open_disk(int tam){
    DIR * cwd=opendir(".");
    struct dirent *dir;
    int fileDesc=0;
    struct disco * ayuda;
    ayuda=malloc(sizeof(struct disco));
    if(cwd){
      while ((dir=readdir(cwd)) !=NULL) {
        if (dir->d_type==DT_REG && strstr(dir->d_name,"vhd") !=NULL) {
            fileDesc=open(dir->d_name, O_RDWR,777);
            ayuda->fd=fileDesc;
            char buffer[14];
            read(fileDesc,buffer,14);
            ayuda->tam=tam;
            return ayuda;
        }
      }
        closedir(cwd);
        fileDesc=open("miDisco.vhd",O_CREAT| O_RDWR,777);
        ayuda->tam = tam;
        ayuda-> fd =fileDesc;
        strcpy(ayuda->nombre,"miDisco.vhd\0#");
        char buffer[tam];
        sprintf(buffer, "miDisco.vhd%d#",tam);
        write(fileDesc,buffer,tam);
      return ayuda;
    }
}//open_disk

void copiaArrays(char a[], char b[], int tam){
  for (size_t i = 0; i < tam; i++) {
    b[i]=a[i];
  }
}

void copiaArraysInt(int a[], int b[], int tam){
  for (size_t i = 0; i < tam; i++) {
    b[i]=a[i];
  }
}
struct disco * read_block(struct  disco * disk, int blockn){
  if(disk->tam >BLOCKTAM){
    printf("%s\n","Mensaje muy grande para guarda" );
    return disk;
  }else{
    int f_read = open("miDisco.vhd", O_RDWR);
    printf("leyendo \n" ); 
    lseek(f_read,blockn, SEEK_CUR);
    char buffer [BLOCKTAM];
    read(f_read,buffer,blockn);
    close(f_read); 
    return disk;
  }
  return disk;
}


struct  disco * write_block(struct disco * disk, int blockn, char * buffer){
  int fileDesc=0;
  if(disk->tam >BLOCKTAM){
    printf("%s\n","Mensaje muy grande para guarda" );
    return disk;
  }else{
    int f_read = open("miDisco.vhd", O_WRONLY);//Abro en modo escritura
    fileDesc=open("miDisco.vhd",O_CREAT| O_RDWR);//Abro en modo escritura
    disk->fd =fileDesc;
    printf("Escribiendo \n");
    lseek(f_read,blockn, SEEK_CUR);
    write(f_read,buffer,strlen(buffer)); 
    close(f_read); 
    return disk;
  }
}


struct disco* iniciaArreglo(struct disco * disk){
  int x=sizeof(disk->bitMap);
  for(int i=0 ; i<x;i++){
    disk->bitMap[i]="0";
  }
  
  return disk;
}

struct disco * write_bitMap(struct disco * disk , int position){
  disk->bitMap[position]='1';
  return disk;
}

int * createArrayInt(int size_array){
 if(size_array>0 ) return malloc(size_array * sizeof(int));
 return NULL;
}


struct disco * pull_apart_seccion(struct disco * disk, int *array_supe_block ){
  int seccion1=disk->tam/4;
  int seccion2=seccion1*2;
  int seccion3=seccion1*3;
  
  write_bitMap(disk,0);

  printf("%d\n",disk->bitMap[0]);
  if(disk->bitMap[seccion1] =="0" ){
  
    printf("Creando las secciones\n");
    write_bitMap(disk,seccion1);
    write_bitMap(disk,seccion1+1);
    
    copiaArraysInt(array_supe_block,createArrayInt(2048),2048);

    write_bitMap(disk,seccion2);
    write_bitMap(disk,seccion2+1);
    
    write_bitMap(disk,seccion3);
    write_bitMap(disk,seccion3+1);
  
    write_block(disk,seccion1,"#1");
    write_block(disk,seccion2,"#2");
    write_block(disk,seccion2,"#3");
  }else{
    printf("Ya se crearon las secciones \n");
  }
  
  return disk;
}


int * create_super_blocks(struct disco * disk, int  array){
 int posicionFinal=(sizeof(disk->bitMap)/4)*2;
 int * secciones= createArrayInt(array);
 return secciones; 
}

int assign_section(struct disco * disk, int * array){
  int contador=0;
  for (int i=0;i<sizeof(array);i++){
    
  }
}

struct disco * write_user(struct  disco * disk, char * name_file ){
  int posicionInicial=(sizeof(disk->bitMap)/4) *2;
  char copiaArray [2048];
  copiaArrays(disk->bitMap,copiaArray,2048);
  int contador=0;  
  for(int i=posicionInicial ; i<sizeof(disk->bitMap);i++ ){
      if(copiaArray[i]!="0"){
        for(int j=0 ;j<strlen(name_file);j++ ){
          contador=i;
          copiaArray[contador]="1";
          write_bitMap(disk,contador);
          contador++;
        }
      }
  }
  time_t tiempo = time(0);
  struct tm *tlocal = localtime(&tiempo);
  char output[128];
  strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);
  printf(" Archivo guardado con la fecha");
  printf("%s\n",output);
  write_block(disk,contador,output);  
  copiaArrays(copiaArray,disk->bitMap,2048);
  return disk;  
}



struct disco * write_file_whith_time(struct  disco * disk, char * name_file ){
  int posicionInicial=(sizeof(disk->bitMap)/4) *2;
  char copiaArray [2048];
  copiaArrays(disk->bitMap,copiaArray,2048);
  int contador=0;  
  for(int i=posicionInicial ; i<sizeof(disk->bitMap);i++ ){
      if(copiaArray[i]!="0"){
        for(int j=0 ;j<strlen(name_file);j++ ){
          contador=i;
          copiaArray[contador]="1";
          write_bitMap(disk,contador);
          contador++;
        }
      }
  }
  time_t tiempo = time(0);
  struct tm *tlocal = localtime(&tiempo);
  char output[128];
  strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);
  printf(" Archivo guardado con la fecha");
  printf("%s\n",output);
  write_block(disk,contador,output);  
  copiaArrays(copiaArray,disk->bitMap,2048);
  return disk;  
}

struct disco * write_file_user(struct disco * disk,char * usuario ){
  int posicionInicial=(sizeof(disk->bitMap)/4)*2;
  int posicionMaxima=(sizeof(disk->bitMap)/4)*3;
  char copiaArray [2048];
  copiaArrays(disk->bitMap,copiaArray,2048);
  int contador=0;  
  for(int i=posicionInicial ; i<sizeof(disk->bitMap);i++ ){
      if(copiaArray[i]!="0"){
        for(int j=0 ;j<strlen(usuario) || posicionInicial > posicionMaxima;j++ ){
          contador=i;
          copiaArray[contador]="1";
          write_bitMap(disk,contador);
          contador++;
        }
      }
  }
  write_block(disk,contador,usuario);
  printf("Escribiendo el usuario \n");
  printf(usuario);
  printf("\n");
  copiaArrays(copiaArray,disk->bitMap,2048);
  return disk;  
}

struct disco * write_file(struct disco * disk, char * file){
  int posicionInicial=(sizeof(disk->bitMap)/4)*3;
  char copiaArray [2048];
  copiaArrays(disk->bitMap,copiaArray,2048);
  int contador=0;  
  for(int i=posicionInicial ; i<sizeof(disk->bitMap);i++ ){
      if(copiaArray[i]!="0"){
        for(int j=0 ;j<strlen(file) || posicionInicial > disk->tam ;j++ ){
          contador=i;
          copiaArray[contador]="1";
          write_bitMap(disk,contador);
          contador++;
        }
      }
  }
  write_block(disk,contador,file);
  printf("Escribiendo en la memoria\n");
  printf("Archivo escribiendo");
  printf("\n");
  copiaArrays(copiaArray,disk->bitMap,2048);
  return disk;
}

int main(){
  int * superblock[2048];
  struct disco * prueba=open_disk(1024);
  struct disco * escitura=write_block(prueba,15,"Hola como estas :)");
  struct disco * lectura=read_block (prueba,8);
  pull_apart_seccion(escitura,superblock);
  write_file_whith_time(prueba,"hola");
  write_file_user(prueba,"diego"); 
  return 0;
}
