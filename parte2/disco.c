/**
*Practica 02
*Nombre: Diego Rosado Cabrera
*nmc: 314293804
*/
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <fcntl.h>
#include <stdlib.h>
#define BLOCKTAM 1024
#include <sys/types.h>

struct disco{
  char  nombre [14];
  int fd;
  int tam;
  int bitMap[2048];
  struct superblock * sp[2048];
};

struct file_descriptor{
  char date[128];
  int group;
  int ID_user;
  char name_file[128];
  int position_file[BLOCKTAM];
  struct block * mensajes;
};

struct block{
  int num_descriptor;
  char message[128];
  int id;
};

struct superblock{
  int  number_files;
  int  number_file_descriptor;
  struct file_descriptor fd[BLOCKTAM];

};
struct disco * open_disk(int tam){
    DIR * cwd=opendir(".");
    struct dirent *dir;
    int fileDesc=0;
    struct disco * ayuda;
    ayuda=malloc(sizeof(struct disco));
    if(cwd){
      while ((dir=readdir(cwd)) !=NULL) {
        if (dir->d_type==DT_REG && strstr(dir->d_name,"vhd") !=NULL) {
            fileDesc=open(dir->d_name, O_RDWR,777);
            ayuda->fd=fileDesc;
            char buffer[14];
            read(fileDesc,buffer,14);
            ayuda->tam;
        }
      }
        closedir(cwd);
        fileDesc=open("miDisco.vhd",O_CREAT| O_RDWR,777);
        ayuda->tam = tam;
        ayuda-> fd =fileDesc;
        printf("%d\n", fileDesc );
        strcpy(ayuda->nombre,"miDisco.vhd\0#");
        char buffer[tam];
        sprintf(buffer, "miDisco.vhd%d#",tam);
        write(fileDesc,buffer,tam);
        return ayuda;
    }
}//open_disk


struct disco * read_block(struct  disco * disk, int blockn){
  if(disk->tam >BLOCKTAM){
    printf("%s\n","Mensaje muy grande para guarda" );
    return disk;
  }else{
    int f_read = open("miDisco.vhd", O_RDWR);
    printf("leyendo \n" );
    lseek(f_read,blockn, SEEK_CUR);
    char buffer [BLOCKTAM];
    read(f_read,buffer,blockn);
    printf("%s\n",buffer );
    close(f_read);
    return disk;
  }
  return disk;
}


struct  disco * write_block(struct disco * disk, int blockn, char * buffer){
  int fileDesc=0;
  if(disk->tam >BLOCKTAM){
    printf("%s\n","Mensaje muy grande para guarda" );
    return disk;
  }else{
    fileDesc=open("miDisco.vhd",O_CREAT| O_RDWR);//Abro en modo escritura
    int f_read = open("miDisco.vhd", O_WRONLY);//Abro en modo escritura
    disk->fd =fileDesc;
    printf("Escribiendo \n");
    lseek(f_read,blockn, SEEK_CUR);

    write(f_read,buffer,strlen(buffer));

    close(f_read);

    return disk;
  }
}

struct disco* initializeBitMap(struct disco * disk){
  for (size_t i = 0; i < sizeof(disk->bitMap); i++) {
    disk->bitMap[i]=0;
  }
  return disk;
}

struct superblock * initizializeSuperBlock(){

  struct superblock * super=malloc(sizeof(struct superblock));
  super->number_files=0;
  return super;
}

void copiaArraysChar(char a[], char b[], int tam){

  for (size_t i = 0; i < tam; i++) {
    printf("%d\n", i );
    b[i]=a[i];
  }
}
struct file_descriptor * initialize_file_descriptor (int user
                                                    ,int group
                                                    ,struct superblock * sb,
                                                    char name[])
{

    struct file_descriptor* fd=malloc(sizeof(struct file_descriptor));
    int number_file_descriptor=sb->number_file_descriptor;
    fd->ID_user=user;
    fd->group=group;
    //fd->position_file=number_file;
    if (!(sizeof(name)<128)) {
      printf("%s\n", "nombre muy largo no valido" );
      return NULL;
    }
    printf("%s\n","Nombre guardado como " );

    strcpy(fd->name_file,name);
    printf("%s\n",name );
    time_t tiempo = time(0);
    struct tm *tlocal = localtime(&tiempo);
    strcpy(fd->name_file,name);
    char output[128];
    printf("%s\n", fd->name_file, "\n"  );
    printf("Archivo guardado con la fecha  \n");
    strcpy(fd->date,output);
    strftime(fd->date,128,"%d/%m/%y %H:%M:%S",tlocal);
    printf("%s\n",fd->date);

    number_file_descriptor++;

    return fd;
}


struct block  createBlock(struct superblock * sp,
                          struct file_descriptor * fd ,
                          char name [],char message [])
{

  char auxiliar[128];
  int number_file=sp->number_files;
  sp->fd->mensajes=malloc(sizeof(struct block));
  sp->fd->mensajes->id=number_file;
  printf("%s\n","Archivo guardado como ");
  strcpy(sp->fd->mensajes->message,name);
  printf("%s\n", sp->fd->mensajes->message);
  printf("%s\n","Guardando mensaje");
  strcpy(sp->fd->mensajes,message);
  printf("%s\n", sp->fd->mensajes->message);
  printf("%s\n","Identificador del archivo" );
  sp->fd->mensajes  ->id=number_file;
  printf("%d\n",sp->fd->mensajes->id );
  fd->position_file[number_file];
  number_file++;
  return sp->fd->mensajes[number_file];
}

int devuelveIndice(struct disco * disk ){

  for (int i = 0; i < sizeof(disk->bitMap); i++) {
    printf("%d\n",disk->bitMap[i] );
    if (disk->bitMap[i]==0) {
      return i;
    }
  }
  return 0;
}
void escribiendoBitMap(struct disco * disk, char  archivo[]){
  int posicion=devuelveIndice(disk);
  for (int i = 0; i <sizeof (archivo) ; i++) {

      disk->bitMap[ posicion+i]=1;
  }
}

struct file_descriptor * mkdir(
                         int user,
                         int group,
                         char name[],
                        struct disco * disk)
{
  int posicion= devuelveIndice(disk);
  disk->bitMap[posicion]=1;
disk->sp[posicion]=malloc(sizeof(struct superblock));

escribiendoBitMap(disk,name);
write_block(disk,posicion,name);
return initialize_file_descriptor(user,group,name,disk->sp[posicion]);
}

void write_in_block(struct disco * disk, struct  file_descriptor * fd,
                  char mensaje[])
{
  fd->mensajes=malloc(sizeof(struct block));
  printf("%s\n","escribiendo mensaje" );
  int  posicion=devuelveIndice(disk);
  escribiendoBitMap(disk,mensaje);
  printf("%s\n","Guardando mensaje" );
  write_block(disk,posicion,mensaje);
  strcpy(fd->mensajes->message ,mensaje);
  printf("%s\n",fd->mensajes->message );
}

void insert_file_in_superblock(struct disco* disk,
                              struct  superblock * sp,
                              struct file_descriptor * fd)
{
  printf("Entre");
  
  if (sp->number_file_descriptor==NULL) {  
    sp->number_file_descriptor=0;
    sp->number_files=0;
  }
  
  int posicion=sp->number_file_descriptor;
  strcpy(sp->fd[posicion].date,fd->date);
  
  sp->fd[posicion].group=fd->group;
  
  sp->fd[posicion].ID_user=fd->ID_user;
  
  strcpy(sp->fd[posicion].name_file,fd->name_file);
  
  sp->fd[posicion].mensajes->message;
  char x[128];
  //fd->mensajes->message  = malloc(sizeof(struct block)); 
  //strcpy(x,fd->mensajes->message);
  //fd->mensajes->message;
  //strcpy(sp->fd[posicion].mensajes->message,fd->mensajes->message);
  sp->number_file_descriptor++;
  sp->number_files++;
  }

void ls(struct disco * disk,struct  superblock * sp ) {
  //printf("Entre");
  for(int i=0; i< sizeof(sp->fd);i++ ){
    if(sp->fd[i].group==NULL){
      printf("%s\n","Entre \n");
      printf( sp->fd[i].name_file);
      printf("Hola");
    }
    printf("%s\n"," no entre");
    break;
  }
}








int main(){
  struct disco * prueba=open_disk(1024);
  struct superblock* prueba3=initizializeSuperBlock();
  struct  file_descriptor *prueba2=mkdir(1,1,"Diego",prueba);
  insert_file_in_superblock(prueba,prueba3,prueba2);
  ls(prueba,prueba3);
  return 0;
}
